import os
from pathlib import Path

from dotenv import find_dotenv, load_dotenv
from emotion_net_infer import EmotionNet


def read_env_variables():
    load_dotenv(find_dotenv())


def chdir_to_projects_root():
    project_dir = Path(find_dotenv()).parent
    os.chdir(project_dir)


if __name__ == "__main__":
    # сменим директорию на корневой каталог проекта
    chdir_to_projects_root()

    # считаем переменные в рабочее окружение
    read_env_variables()
    
    # сохраним значение из переменной окружения MLFLOW_SERVER_URI в
    # переменную
    mlflow_server_uri = os.getenv("MLFLOW_SERVER_URI")

    # инициализируем объект для инференса нейросети
    emotion_net = EmotionNet(mlflow_server_uri)

    # укажем путь к тестовой картинке
    path_to_image = "repo_pics/example_for_emotion_recognition.png"

    # осуществим предсказания на указанном изображении
    emotion_cls, score = emotion_net.load_image_and_predict(path_to_image, True)

    # вывод результатов
    print(emotion_cls, score)

    # результат предсказания:
    # neutral 0.4857593774795532