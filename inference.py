# Инференс модели
import os
from pathlib import Path

import mlflow
from dotenv import find_dotenv, load_dotenv
from emotion_det_net_infer import DetectEmotionOnFaces
from emotion_net_infer import EmotionSeqClassifier
from emotion_video_track import EmotionVideoTrack
from fastapi import FastAPI, File, HTTPException, UploadFile
from fastapi.responses import FileResponse


def read_env_variables():
    load_dotenv(find_dotenv())


def chdir_to_projects_root():
    project_dir = Path(find_dotenv()).parent
    os.chdir(project_dir)


# сменим директорию на корневой каталог проекта
chdir_to_projects_root()

# считаем переменные в рабочее окружение
read_env_variables()
mlflow_server_uri = os.getenv("MLFLOW_SERVER_URI")
mlflow.set_tracking_uri(mlflow_server_uri)

# инициализация приложения FastAPI
app = FastAPI()

emo_seq_classifier = EmotionSeqClassifier()
emotion_det_net = DetectEmotionOnFaces(emo_seq_classifier)
emotion_video_track = EmotionVideoTrack()

@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    # Handle the file only if it is a .png
    if file.filename.endswith(".png") or file.filename.endswith(".jpg"):
        contents = file.file.read()
        with open(file.filename, "wb") as f:
            f.write(contents)

        detections, image_rgb = emotion_det_net.load_image_and_detect_faces(file.filename)
        image_rgb = emotion_det_net.viz_emo_detections(image_rgb, detections)
    
        image_rgb.save("test_image.jpg")

        return FileResponse("test_image.jpg")
    
    if file.filename.endswith(".mp4"):
        contents = file.file.read()
        with open(file.filename, "wb") as f:
            f.write(contents)
            
        output_video_emotion_annot = "video_emo_annotation.csv"
        output_video_emotion_viz = "video_emo_viz.mp4"

        emotion_video_track.track_emotions_on_video(
            file.filename, "output", output_video_emotion_annot, output_video_emotion_viz
        )
        
        return FileResponse("output/video_emo_annotation.csv")

    else:
        raise HTTPException(
            status_code=400, default="Неправильный формат файла. Разрешены .jpg и .png"
        )
        


if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(1)
